import { shallowMount } from '@vue/test-utils';
import Home from '@/views/Home.vue';

const list = [
  {
    id: 1,
    description: 'test description',
    name: 'test name',
    date: 'test date',
    isDone: false,
  },
  {
    id: 2,
    description: 'test description',
    name: 'test name',
    date: 'test date',
    isDone: false,
  },
  {
    id: 3,
    description: 'test description',
    name: 'test name',
    date: 'test date',
    isDone: false,
  },
];

describe('Home component', () => {
  beforeEach(() => {
    global.fetch = jest.fn();
    global.fetch.mockReturnValue(Promise.resolve({
      json: () => Promise.resolve([...list]),
    }));
  });

  afterEach(() => {
    global.fetch.mockClear();
    delete global.fetch;
  });

  describe('when saving item', () => {
    it('should add name', () => {
      const wrapper = shallowMount(Home, {
        beforeMount: () => {
        },
        data: () => ({
          todoItemName: '',
          todoItemDescription: '',
          list: [
            {
              id: 1,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 2,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 3,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
          ],
        }),
      });
      const expectedName = 'test';
      const input = wrapper.find('.todo-item__text--name');
      input.element.value = 'test';
      input.trigger('input');
      expect(wrapper.vm.$data.todoItemName)
        .toBe(expectedName);
    });
    it('should add description', () => {
      const wrapper = shallowMount(Home, {
        beforeMount: () => {
        },
        data: () => ({
          todoItemName: '',
          todoItemDescription: '',
          list: [
            {
              id: 1,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 2,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 3,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
          ],
        }),
      });
      const expectedDescription = 'test';
      const input = wrapper.find('.todo-item__text--description');
      input.element.value = 'test';
      input.trigger('input');
      expect(wrapper.vm.$data.todoItemDescription)
        .toBe(expectedDescription);
    });
    it('should call add data', () => {
      const wrapper = shallowMount(Home, {
        beforeMount: () => {
        },
        data: () => ({
          todoItemName: '',
          todoItemDescription: '',
          list: [
            {
              id: 1,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 2,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 3,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
          ],
        }),
      });
      const saveStub = jest.fn();
      wrapper.setMethods({
        addItem: saveStub,
      });
      wrapper.find('.todo-item__create-btn')
        .trigger('click');
      expect(saveStub)
        .toHaveBeenCalled();
    });
    it('should send request for data', (done) => {
      const wrapper = shallowMount(Home, {
        beforeMount: () => {
        },
        data: () => ({
          todoItemName: '',
          todoItemDescription: '',
          list: [
            {
              id: 1,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 2,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
            {
              id: 3,
              description: 'test description',
              name: 'test name',
              date: 'test date',
              isDone: false,
            },
          ],
        }),
      });
      const testItem = {
        name: 'test',
        description: 'test',
        isDone: false,
        date: 'test',
      };
      global.fetch.mockReturnValue(Promise.resolve({
        json: () => Promise.resolve(testItem),
      }));
      wrapper.vm.addItem();
      setTimeout(() => {
        expect(wrapper.vm.$data.list[wrapper.vm.$data.list.length - 1])
          .toEqual(testItem);
        done();
      });
    });
  });
  it('should call fetch on item remove and update list', (done) => {
    const wrapper = shallowMount(Home, {
      beforeMount: () => {
      },
      data: () => ({
        todoItemName: '',
        todoItemDescription: '',
        list: [
          {
            id: 1,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 2,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 3,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
        ],
      }),
    });
    const endList = list
      .filter(item => item.id !== 1);
    global.fetch.mockReturnValue(Promise.resolve({
      json: () => Promise.resolve(endList),
    }));
    wrapper.vm.deleteItem();
    expect(endList.length)
      .toBe(list.length - 1);
    setTimeout(() => {
      expect(endList)
        .toEqual(wrapper.vm.$data.list);
      done();
    });
  });
  it('should mark item as done and update list', (done) => {
    const wrapper = shallowMount(Home, {
      beforeMount: () => {
      },
      data: () => ({
        todoItemName: '',
        todoItemDescription: '',
        list: [
          {
            id: 1,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 2,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 3,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
        ],
      }),
    });
    const endList = list
      .filter(item => item.id !== 1);
    global.fetch.mockReturnValue(Promise.resolve({
      json: () => Promise.resolve(endList),
    }));
    wrapper.vm.markAsDone();
    expect(endList.length)
      .toBe(list.length - 1);
    setTimeout(() => {
      expect(endList)
        .toEqual(wrapper.vm.$data.list);
      done();
    });
  });
});
