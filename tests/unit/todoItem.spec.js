import { shallowMount, mount } from '@vue/test-utils';
import TodoItem from '@/components/todoItem.vue';

describe('todoItem component', () => {
  it('should emit remove todo with todo id on remove btn click', () => {
    const stub = jest.fn();
    const wrapper = shallowMount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: false,
        },
      },
    });
    wrapper.setMethods({
      onRemoveClick: stub,
    });
    wrapper.find('.todo-item__btn--remove').trigger('click');
    expect(stub).toBeCalled();
  });

  it('should emit remove todo with todo id on restore btn click', () => {
    const stub = jest.fn();
    const wrapper = shallowMount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: true,
        },
      },
    });
    wrapper.setMethods({
      onRestoreClick: stub,
    });
    wrapper.find('.todo-item__btn--restore').trigger('click');
    expect(stub).toBeCalled();
  });

  it('should emit remove todo with todo id on done btn click', () => {
    const stub = jest.fn();
    const wrapper = mount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: false,
        },
      },
    });
    wrapper.setMethods({
      onDoneClick: stub,
    });
    wrapper.find('.todo-item__btn--done').trigger('click');
    expect(stub).toBeCalled();
  });

  it('should hide restore btn if isDone is false', () => {
    const wrapper = mount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: false,
        },
      },
    });
    expect(wrapper.find('.todo-item__btn--restore').exists()).toBe(false);
  });

  it('should hide done btn if isDone is true', () => {
    const wrapper = mount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: true,
        },
      },
    });
    expect(wrapper.find('.todo-item__btn--done').exists()).toBe(false);
  });

  it('should emit correct event and data onDoneClick', () => {
    const wrapper = mount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: true,
        },
      },
    });
    wrapper.vm.onDoneClick();
    expect(wrapper.emitted().markAsDone).toBeTruthy();
    expect(wrapper.emitted().markAsDone[0]).toEqual([1234]);
  });

  it('should emit correct event and data onRestoreClick', () => {
    const wrapper = mount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: true,
        },
      },
    });
    wrapper.vm.onRestoreClick();
    expect(wrapper.emitted().restoreFromArchive).toBeTruthy();
    expect(wrapper.emitted().restoreFromArchive[0]).toEqual([1234]);
  });



  it('should emit correct event and data onRestoreClick', () => {
    const wrapper = mount(TodoItem, {
      propsData: {
        todo: {
          id: 1234,
          description: 'test description',
          name: 'test name',
          date: 'test date',
          isDone: true,
        },
      },
    });
    wrapper.vm.onRemoveClick();
    expect(wrapper.emitted().removeToDo).toBeTruthy();
    expect(wrapper.emitted().removeToDo[0]).toEqual([1234]);
  });
});
