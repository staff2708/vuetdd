import { shallowMount } from '@vue/test-utils';
import Archive from '@/views/Archive.vue';

const list = [
  {
    id: 1,
    description: 'test description',
    name: 'test name',
    date: 'test date',
    isDone: false,
  },
  {
    id: 2,
    description: 'test description',
    name: 'test name',
    date: 'test date',
    isDone: false,
  },
  {
    id: 3,
    description: 'test description',
    name: 'test name',
    date: 'test date',
    isDone: false,
  },
];

describe('archive component' , () => {
  beforeEach(() => {
    global.fetch = jest.fn();
    global.fetch.mockReturnValue(Promise.resolve({
      json: () => Promise.resolve([...list]),
    }));
  });

  afterEach(() => {
    global.fetch.mockClear();
    delete global.fetch;
  });

  it('should correct delete item', (done) => {
    const wrapper = shallowMount(Archive, {
      beforeMount: () => {
      },
      data: () => ({
        list: [
          {
            id: 1,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 2,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 3,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
        ],
      }),
    });
    const endList = list
      .filter(item => item.id !== 1);
    global.fetch.mockReturnValue(Promise.resolve({
      json: () => Promise.resolve(endList),
    }));
    wrapper.vm.onItemDelete();
    expect(endList.length)
      .toBe(list.length - 1);
    setTimeout(() => {
      expect(endList)
        .toEqual(wrapper.vm.$data.list);
      done();
    });
  });
  it('should correct restore item', (done) => {
    const wrapper = shallowMount(Archive, {
      beforeMount: () => {
      },
      data: () => ({
        list: [
          {
            id: 1,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 2,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 3,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
        ],
      }),
    });
    const endList = list
      .filter(item => item.id !== 1);
    global.fetch.mockReturnValue(Promise.resolve({
      json: () => Promise.resolve(endList),
    }));
    wrapper.vm.onItemRestore();
    expect(endList.length)
      .toBe(list.length - 1);
    setTimeout(() => {
      expect(endList)
        .toEqual(wrapper.vm.$data.list);
      done();
    });
  });
});
