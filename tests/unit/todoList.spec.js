import { mount } from '@vue/test-utils';
import TodoList from '@/components/todoList.vue';


describe('todoList component', () => {
  it('should hide restore btn if isDone is false', () => {
    const wrapper = mount(TodoList, {
      propsData: {
        deleteItem: jest.fn(),
        markAsDone: jest.fn(),
        list: [
          {
            id: 1,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 2,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 3,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
        ],
      },
    });
    expect(wrapper.findAll('.todo-list__item-wrapper').length).toBe(wrapper.vm.$props.list.length);
  });
  it('should call onItemDelete if removeToDo was fired', () => {
    const wrapper = mount(TodoList, {
      propsData: {
        deleteItem: jest.fn(),
        markAsDone: jest.fn(),
        list: [
          {
            id: 1,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 2,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 3,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
        ],
      },
    });
    wrapper.find('.todo-list__item-wrapper > *').vm.$emit('removeToDo');
    expect(wrapper.vm.deleteItem).toHaveBeenCalled();
  });
  it('should call onItemDone if markAsDone was fired', () => {
    const wrapper = mount(TodoList, {
      propsData: {
        deleteItem: jest.fn(),
        markAsDone: jest.fn(),
        list: [
          {
            id: 1,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 2,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
          {
            id: 3,
            description: 'test description',
            name: 'test name',
            date: 'test date',
            isDone: false,
          },
        ],
      },
    });
    wrapper.find('.todo-list__item-wrapper > *').vm.$emit('markAsDone');
    expect(wrapper.vm.markAsDone).toHaveBeenCalled();
  });
});
