const express = require('express');
const uuid = require('uuid/v4');
const corsMiddleware = require('./middlewares/corsMiddleware');

const DB = {
  todo: [],
  archive: [],
};

const app = express();

app.use((req, res, next) => {
  let data = '';
  req.on('data', (chunk) => {
    data += chunk;
  });
  req.on('end', () => {
    req.rawBody = data;
    if (data && data.indexOf('{') > -1) {
      req.body = JSON.parse(data);
    }
    next();
  });
});
app.use(express.urlencoded({ extended: true }));
app.use(corsMiddleware);

app.get('/todo', (req, res) => {
  res.send(DB.todo);
});

app.post('/todo/add', (req, res) => {
  const {
    name, description, isDone, date,
  } = req.body;
  const id = uuid();
  DB.todo.push({
    id,
    name,
    description,
    isDone,
    date,
  });
  res.send(DB.todo[DB.todo.length - 1]);
});

app.delete('/todo/:id/remove', (req, res) => {
  const { id } = req.params;
  DB.todo = DB.todo.filter(item => item.id !== id);
  res.send(DB.todo);
});


app.delete('/archive/:id/remove', (req, res) => {
  const { id } = req.params;
  DB.archive = DB.archive.filter(item => item.id !== id);
  res.send(DB.archive);
});


app.put('/todo/:id/complete', (req, res) => {
  const { id } = req.params;
  const foundedTodo = DB.todo.find(item => item.id === id);
  foundedTodo.isDone = true;
  DB.archive.push(foundedTodo);
  DB.todo = DB.todo.filter(item => item.id !== id);
  res.send(DB.todo);
});

app.put('/archive/:id/restore', (req, res) => {
  const { id } = req.params;
  const foundedTodo = DB.archive.find(item => item.id === id);
  foundedTodo.isDone = false;
  DB.todo.push(foundedTodo);
  DB.archive = DB.archive.filter(item => item.id !== id);
  res.send(DB.archive);
});

app.get('/archive', (req, res) => {
  res.send(DB.archive);
});

app.listen(4200, () => {
  console.log('server listen on port 4200');
});
